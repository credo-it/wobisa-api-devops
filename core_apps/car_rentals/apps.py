from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class CarRentalsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'core_apps.car_rentals'
    verbose_name = _('Car Rentals')
