from rest_framework import permissions, status
from rest_framework.exceptions import NotFound
from rest_framework.response import Response
from rest_framework.views import APIView

from core_apps.listings.models import Listing
from .models import Reaction
from .serializers import ReationSerializer


def find_listing_helper(slug):
    try:
        listing = Listing.objects.get(slug=slug)
    except Listing.DoesNotExist:
        raise NotFound(f'Listing with this slug {slug} does not exist.')
    return listing


class ReactionAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = ReationSerializer

    def set_reaction(self, listing, user, reation):
        try:
            exist_reaction = Reaction.object.get(listing=listing, user=user)
            exist_reaction.delete()
        except Reaction.DoesNotExist:
            pass
        data = {'listing': listing.pkid,
                'user': user.pkid, 'reaction': reation}
        serializer = self.serializer_class(data=data)
        serializer.is_valid(raise_execption=True)
        serializer.save()

        response = {'message': 'Reaction successfully set'}
        status_code = status.status.HTTP_201_CREATED
        return response, status_code

    def post(self, request, *args, **kwargs):
        slug = self.kwargs.get('slug')
        listing = find_listing_helper(slug)
        user = request.user
        reaction = request.data.get('reaction')

        try:
            existing_same_reaction = Reaction.object.get(
                listing=listing, user=user, reaction=reaction)
            existing_same_reaction.delete()
            response = {
                'message': f'You no-longer {"LIKE" if reaction in [1, "1"] else "DISLIKE"}'
            }
            status_code = status.HTTP_200_OK
        except Reaction.DoesNotExist:
            response, status_code = self.set_reaction(listing, user, reaction)
        return Response(response, status_code)
