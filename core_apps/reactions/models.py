from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import gettext_lazy as _

from core_apps.common.models import TimeStampedUUIDModel
from core_apps.listings.models import Listing

User = get_user_model()


class ReactionManager(models.Manager):
    def likes(self):
        return self.get_queryset().filter(reaction__gt=0).count()

    def dislikes(self):
        return self.get_queryset().filter(reaction__lt=0).count()

    def has_reacted(self):
        request = self.context.get('request')
        if request:
            self.get_queryset().filter(user=request)


class Reaction(TimeStampedUUIDModel):
    class Reations(models.IntegerChoices):
        LIKE = 1, _('like')
        DISLIKE = -1, _('dislike')

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    listing = models.ForeignKey(
        Listing, on_delete=models.CASCADE, related_name='listing_reactions')
    reation = models.IntegerField(verbose_name=_(
        'like-dislike'), choices=Reations.choices)

    object = ReactionManager()

    class Meta:
        unique_together = ['user', 'listing', 'reation']

    def __str__(self):
        return (
            f'{self.user.username} voted on {self.listing.title} with {self.reaction}'
        )
