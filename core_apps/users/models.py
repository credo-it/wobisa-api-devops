import uuid

from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from .managers import CustomerUserManager
from .validators import validate_file_size


class User(AbstractBaseUser, PermissionsMixin):
    pkid = models.BigAutoField(primary_key=True, editable=False)
    id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    username = models.CharField(
        verbose_name=_('username'), db_index=True, max_length=255, unique=True
    )
    first_name = models.CharField(verbose_name=_('first name'), max_length=255)
    last_name = models.CharField(verbose_name=_('last name'), max_length=255)
    email = models.EmailField(verbose_name=_(
        'email address'), db_index=True, unique=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_customer = models.BooleanField(default=True)
    is_service_provider = models.BooleanField(default=False)
    id_type = models.CharField(max_length=255)
    id_image_url = models.CharField(max_length=255)
    profile_picture_url = models.CharField(max_length=50)
    date_joined = models.DateTimeField(auto_now_add=True)
    location_long = models.DecimalField(
        max_digits=6, decimal_places=2, default=1142)
    location_lat = models.DecimalField(
        max_digits=6, decimal_places=2, default=1142)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'first_name', 'last_name']

    objects = CustomerUserManager()

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __str__(self):
        return self.username

    @property
    def get_full_name(self):
        return f'{self.first_name.title()} {self.last_name.title()}'

    @property
    def get_short_name(self):
        return self.first_name

    def get_is_service_provider(self, user):
        if self.provider.filter(id=user.pkid).exists():
            return True
        else:
            return False
