from django.urls import path
from .views import create_listing_rating_view

urlpatterns = [
    path('<str:listing_id>/', create_listing_rating_view, name='rate-listing')
]
