from rest_framework import generics, permissions, status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from core_apps.listings.models import Listing

from .exceptions import ALreadyRated, CantRateYourOwnListing
from .models import Rating


@api_view(['POST'])
@permission_classes([permissions.IsAuthenticated])
def create_listing_rating_view(request, listing_id):
    owner = request.user
    listing = Listing.objects.get(id=listing_id)
    data = request.data

    if listing.owner == owner:
        raise CantRateYourOwnListing

    already_exists = listing.listing_ratings.filter(
        rated_by__pkid=owner.pkid)
    if already_exists:
        raise ALreadyRated
    elif data['value'] == 0:
        formatted_response = {'detail': 'You can\'nt give a zero rating.'}
        return Response(formatted_response, status=status.HTTP_400_BAD_REQUEST)
    else:
        rating = Rating.objects.create(
            listing=listing,
            rated_by=request.user,
            value=data['value'],
            comment=data['comment']
        )

    return Response({'SUCCESS': 'Rating has been added'}, status=status.HTTP_200_OK)
