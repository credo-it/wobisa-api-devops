from rest_framework.exceptions import APIException


class CantRateYourOwnListing(APIException):
    status_code = 403
    default_detail = 'You can\'t rate/review your own listing'


class ALreadyRated(APIException):
    status_code = 400
    default_detail = 'You have already rated this listing'
