from django.contrib.auth import get_user_model
from rest_framework import serializers
from .models import Review

User = get_user_model()


class ReviewSerializer(serializers.ModelSerializer):
    created_at = serializers.SerializerMethodField()
    updated_at = serializers.SerializerMethodField()

    def get_created_at(self, obj):
        now = obj.created_at
        formatted_date = now.strftime('%m/%d/%Y, %H:%M:%S')
        return formatted_date

    def get_updated_at(self, obj):
        then = obj.updated_at
        formatted_date = then.strftime('%m/%d/%Y, $H:%M:%S')
        return formatted_date

    class Meta:
        model = Review
        fields = ['id', 'owner', 'listing',
                  'body', 'created_at', 'updated_at']


class ReviewListSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.user.username')
    listing = serializers.ReadOnlyField(source='listing.title')
    created_at = serializers.SerializerMethodField()
    updated_at = serializers.SerializerMethodField()

    def get_created_at(self, obj):
        now = obj.created_at
        formatted_date = now.strftime('%m/%d/%Y, %H:%M:%S')
        return formatted_date

    def get_updated_at(self, obj):
        then = obj.updated_at
        formatted_date = then.strftime('%m/%d/%Y, $H:%M:%S')
        return formatted_date

    class Meta:
        model = Review
        fields = ['id', 'owner', 'listing',
                  'body', 'created_at', 'updated_at']
