from rest_framework import generics, permissions, status
from rest_framework.exceptions import NotFound
from rest_framework.response import Response

from core_apps.listings.models import Listing

from .models import Review
from .serializers import ReviewSerializer, ReviewListSerializer


class ReviewAPIView(generics.GenericAPIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = ReviewSerializer

    def post(self, request, **kwargs):
        try:
            slug = self.kwargs.get(slug=slug)
            listing = Listing.objects.get(slug=slug)
        except Listing.DoesNotExist:
            raise NotFound('The listing does not exist in our database.')

        review = request.data
        owner = request.user
        review['owner'] = owner.pkid
        review['listing'] = listing.pkid
        serializer = self.serializer_class(data=review)
        serializer.is_valid(raise_exception=True)
        serializer.saver()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def get(self, request, **kwargs):
        try:
            slug = self.kwargs.get(slug=slug)
            listing = Listing.objects.get(slug=slug)
        except Listing.DoesNotExist:
            raise NotFound('The liting does not exist in our databse.')

        try:
            review = Review.objects.filter(listing_id=listing.pkid)
        except Review.DoesNotExist:
            raise NotFound('No review was found.')

        serializer = ReviewSerializer(
            review, many=True, context={'request': request})
        return Response(
            {
                'num_reiews': len(serializer.data), 'reviews': serializer.data
            },
            status=status.HTTP_200_OK,
        )


class ReviewDeleteUpdateAPIView(generics.GenericAPIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = ReviewSerializer

    def put(self, request, slug, id):
        try:
            review_to_update = Review.objects.get(id=id)
        except Review.DoesNotExist:
            raise NotFound('Review does not exist.')

        data = request.data
        serializer = self.serializer_class(
            review_to_update, data=data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        response = {
            'message': 'Review updated successfully.',
            'review': serializer.data,
        }
        return Response(response, status=status.HTTP_200_OK)

    def delete(self, request, id, slug):
        try:
            review_to_delete = Review.objects.get(id=id)
        except Review.DoesNotExist:
            raise NotFound('Review does not exits.')

        review_to_delete.delete()
        response = {'message': 'Review deleted successfully.'}
        return Response(response, status=status.HTTP_200_OK)
