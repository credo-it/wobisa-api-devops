from django.urls import path
from .views import ReviewAPIView, ReviewDeleteUpdateAPIView

urlpatterns = [
    path('<slug:slug>/review/', ReviewAPIView.as_view(), name='reviews'),
    path('<slug:slug>/review/<str:id>/',
         ReviewDeleteUpdateAPIView.as_view(), name='review')
]
