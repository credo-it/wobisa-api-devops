from django.contrib.auth import get_user_model
from django.db import models
from core_apps.common.models import TimeStampedUUIDModel

User = get_user_model


class Review(TimeStampedUUIDModel):
    listing = models.ForeignKey(
        'listings.Listing', on_delete=models.CASCADE, related_name='reviews')
    owner = models.ForeignKey('profiles.Profile', on_delete=models.CASCADE)
    body = models.TextField()

    def __str__(self):
        return f'{self.owner} left a review for {self.listing}'
