# from django.contrib.auth import get_user_model
# from rest_framework.response import Response
# from rest_framework.decorators import api_view, permission_classes
# from rest_framework import permissions
# import base64
# import requests
# import json
# import hashlib
# import urllib2
# from random import randint
# import environ
# env = environ.Env()

# User = get_user_model()


# @api_view(['POST'])
# @permission_classes([permissions.IsAuthenticated])
# def process_kyc(request, user_pkid):
#     user = request.user
#     user_pkid = user.pkid
#     '''
#     Python 2
#     --------
#     import urllib2

#     Python 3
#     --------
#     import urllib.request
#     urllib.request.urlopen(url).read()
#     '''
#     url = 'https://api.shuftipro.com/'
#     # Your Shufti Pro account Client ID
#     client_id = env('CLIENT_ID')
#     # Your Shufti Pro account Secret Key
#     secret_key = env('SECRET_KEY')
#     # OR Access Token
#     # access_token = 'YOUR-ACCESS-TOKEN';

#     verification_request = {
#         'reference':   'ref-{}{}'.format(randint(1000, 9999), randint(1000, 9999)),
#         'country':   'GB',
#         'language':   'EN',
#         'email':   'test@test.com',
#         'callback_url':   'https://yourdomain.com/profile/notifyCallback',
#         'verification_mode':   'any',
#     }
#     # Use this key if you want to perform document verification
#     verification_request['document'] = {
#         'proof':   base64.b64encode(urllib2.urlopen('https://raw.githubusercontent.com/shuftipro/RESTful-API-v1.3/master/assets/real-id-card.jpg').read()).decode(),
#         'additional_proof':   base64.b64encode(urllib2.urlopen('https://raw.githubusercontent.com/shuftipro/RESTful-API-v1.3/master/assets/real-id-card.jpg').read()).decode(),
#         'name':   '',
#         'dob':   '',
#         'age':   '',
#         'document_number':   '',
#         'expiry_date':   '',
#         'issue_date':   '',
#         'supported_types':   ['id_card', 'passport'],
#         'gender':  ''
#     }
#     # Use this key if you want to perform address verification
#     verification_request['address'] = {
#         'proof':   base64.b64encode(urllib2.urlopen('https://raw.githubusercontent.com/shuftipro/RESTful-API-v1.3/master/assets/real-id-card.jpg').read()).decode(),
#         'name':   '',
#         'full_address':   '',
#         'address_fuzzy_match': '1',
#         'issue_date':   '',
#         'supported_types':   ['utility_bill', 'passport', 'bank_statement']
#     }
#     # Calling Shufti Pro request API using python requests
#     auth = '{}:{}'.format(client_id, secret_key)
#     b64Val = base64.b64encode(auth.encode()).decode()
#     # if access token
#     # b64Val = access_token
#     # replace "Basic with "Bearer" in case of Access Token
#     response = requests.post(url,
#                              headers={"Authorization": "Basic %s" %
#                                       b64Val, "Content-Type": "application/json"},
#                              data=json.dumps(verification_request))

#     # Calculating signature for verification
#     # calculated signature functionality cannot be implement in case of access token
#     calculated_signature = hashlib.sha256('{}{}'.format(
#         response.content.decode(), secret_key).encode()).hexdigest()

#     # Get Shufti Pro Signature
#     sp_signature = response.headers.get('Signature', '')

#     # Convert json string to json object
#     json_response = json.loads(response.content)

#     # Get event returned
#     event_name = json_response['event']
#     print(json_response)
#     if event_name == 'verification.accepted':
#         if sp_signature == calculated_signature:
#             print('Verification accepted: {}'.format(response.content))

#             user = User.objects.get(pkid=user_pkid)
#             user.is_service_provider = True
#         else:
#             print('Invalid signature: {}'.format(response.content))
#     else:
#         print('Error: {}'.format(response.content))

#     return Response({'SUCCESS': 'User ID verification has been done successfully.'})
