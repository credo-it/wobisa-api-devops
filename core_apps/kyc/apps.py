from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class KycAmlConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'core_apps.kyc'
    verbose_name = _('Know Your Customer')
