from django.contrib import admin
from .models_old import KycApplication, KYCSetting


@admin.register(KycApplication)
class KYCAdmin(admin.ModelAdmin):

    def has_add_permission(self, obj):
        return False


@admin.register(KYCSetting)
class KYCSettingAdmin(admin.ModelAdmin):
    pass
