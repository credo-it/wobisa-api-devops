from rest_framework import generics, permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView

from core_apps.listings.models import Listing
from core_apps.listings.serializers import ListingCreateSerializer

from .exceptions import AlreadyFavourited
from .models import Favourite
from .serializers import FavouriteSerializer


class FavouriteAPIView(generics.CreateAPIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = FavouriteSerializer

    def post(self, request, slug):
        data = request.data
        listing = Listing.objects.get(slug, slug)
        user = request.user

        favourite = Favourite.objects.filter(
            user=user, listing=listing.pkid).first()

        if favourite:
            raise AlreadyFavourited
        else:
            data['listing'] = listing.pkid
            data['user'] = user.pkid
            serializer = self.get_serializer(data=data)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            data = serializer.data
            data['message'] = 'Listing Added to favourites.'
            return Response(data, status=status.HTTP_201_CREATED)


class ListUserFavouriteListingsAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        favourites = Favourite.objects.filter(user_id=request.usesr.pkid)

        favourite_listings = []
        for favourite in favourites:
            listing = Listing.objects.get(pkid=favourite.listing.pkid)
            listing = ListingCreateSerializer(
                listing, context={'listing': listing.slug, 'request': request}).data
            favourite_listings.append(listing)
        favourites = {'my_favourites': favourite_listings}
        return Response(data=favourites, status=status.HTTP_200_OK)
