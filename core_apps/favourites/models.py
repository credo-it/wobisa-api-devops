from django.contrib.auth import get_user_model
from django.db import models

from core_apps.listings.models import Listing
from core_apps.common.models import TimeStampedUUIDModel

User = get_user_model()


class Favourite(TimeStampedUUIDModel):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='favourites')
    listing = models.ForeignKey(
        Listing, on_delete=models.CASCADE, related_name='listing_favourites')

    def __str__(self):
        return f'{self.user.username} favourite {self.listing.title}'

    def is_favourited(self, user, listing):
        try:
            listing = self.listing
            user = self.user
        except Listing.DoesNotExist:
            pass

        queryset = Favourite.objects.filter(listing_id=listing, user_id=user)

        if queryset:
            return True
        return False
