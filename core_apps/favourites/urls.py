from django.urls import path
from . import views

urlpatterns = [
    path('listing/me/', views.ListUserFavouriteListingsAPIView.as_view,
         name='my-favourites',),
    path('<slug:slug>./', views.FavouriteAPIView.as_view(), name='favourite-listing')
]
