from drf_haystack.serializers import HaystackSerializer

from core_apps.search.search_indexes import ListingIndex


class ListingSearchSerializer(HaystackSerializer):
    class Meta:
        index_classes = [ListingIndex]

        fields = ['title', 'city', 'rate',
                  'autocomplete', 'created_at', 'updated_at']
        ignore_fields = ['autocomplete']
        field_aliases = {'q': 'autocomplete'}
