from django.urls import path
from rest_framework import routers

from .views import SearchListingsView

router = routers.DefaultRouter()
router.register('search/', SearchListingsView, basename='search-listing')

urlpatterns = [
    path(
        'search/', SearchListingsView.as_view({'get': 'list'}), name='search-listing')
]
