from drf_haystack import viewsets
from drf_haystack.filters import HaystackAutocompleteFilter
from rest_framework import permissions
from core_apps.listings.models import Listing
from .serializers import ListingSearchSerializer


class SearchListingsView(viewsets.HaystackViewSet):
    permission_classes = [permissions.AllowAny]
    index_models = [Listing]
    serializer_class = ListingSearchSerializer
    filter_backends = [HaystackAutocompleteFilter]
