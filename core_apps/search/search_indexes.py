from django.utils import timezone
from haystack import indexes

from core_apps.listings.models import Listing


class ListingIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True)
    title = indexes.CharField(model_attr='title')
    city = indexes.CharField(model_attr='city')
    rate = indexes.DecimalField(model_attr='rate')

    @staticmethod
    def prepare_owner(obj):
        return '' if not obj.owner else obj.owner.username

    @staticmethod
    def prepare_autocomplete(obj):
        return ' '.join((obj.owner.username, obj.title, obj.description))

    @staticmethod
    def index_queryset(self, using=None):
        return self.get_model().objects.filter(created_at__lte=timezone())

    def get_model(self):
        return Listing
