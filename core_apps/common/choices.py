from django.utils.translation import gettext_lazy as _


class ModelChoices:

    # ////// Proof of address choices ///////////
    BANK_STATEMENT = 'BANK_STATEMENT'
    CREDIT_CARD_STATEMENT = 'CREDIT_CARD_STATEMENT'
    UTILITY = 'UTILITY'

    PROOF_OF_ADDRESS_TYPE = (
        (BANK_STATEMENT, _('Bank Statement')),
        (CREDIT_CARD_STATEMENT, _('Credit Card Statement')),
        (UTILITY, _('Utility')),
    )

    # ////// Photo identification type //////////
    PHOTO_IDENTIFICATION_TYPE = (
        ('national_id', _('National ID')),
        ('passport', _('Passport')),
        ('drivers license', _('Drivers License')),
    )

    # ///// KYC STATUS //////////////////////
    KYC_SATUS = (
        ('verified', _('Verified')),
        ('unverified', _('unverified')),
        ('pending', _('Pending')),
        ('rejected', _('Pending')),
        ('cancelled', _('Cancelled')),
    )

    # /////// PEP CHOICES ////////////////////
    # For people who are politically exposed
    PEP_CHOICES = (
        ('pep', _('I\'m polically exposed')),
        ('non_pep', _('No, I\'m not politically exposed'))
    )

    # KYC REFUSAL REASON CODE
    KYC_REFUSAL_REASON_CODE = (
        ('EXPIRED_DOCUMENT', _('Document Expired')),
        ('DOCUMENT_DOES_NOT_MATCH_USER_DATA', _(
            'Documenet does not match user data')),
    )

    # ///// PHOTO IDENTIFICATION TYPE //////////
    