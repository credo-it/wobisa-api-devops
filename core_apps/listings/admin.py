from django.contrib import admin
from . import models


@admin.register(models.Listing)
class ListingAdmin(admin.ModelAdmin):
    list_display = ['pkid', 'title', 'owner', 'slug',
                    'city', 'rate', 'views', 'description', 'amenities', 'status']
    list_display_links = ['pkid', 'owner']


@admin.register(models.ListingCategory)
class ListingGategoryAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'description']
