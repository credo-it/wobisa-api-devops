from rest_framework.pagination import PageNumberPagination


class ListingPagination(PageNumberPagination):
    page_size = 5
