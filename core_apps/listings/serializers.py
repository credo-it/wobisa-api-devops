from rest_framework import serializers
from core_apps.listings.models import (
    Listing, ListingViews,
    ListingImage, Amenities
)
from core_apps.reviews.serializers import ReviewListSerializer
from core_apps.ratings.serializers import RatingSerializer

from .custom_tags_field import TagRelatedField


class ListingViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = ListingViews
        exclude = ['updated_at', 'pkid']


class ListingImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ListingImage
        fields = ['product_image']


class AmenitiesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Amenities
        fields = ['type', 'quantities', 'description']


class ListingSerializer(serializers.ModelSerializer):
    owner_info = serializers.SerializerMethodField(read_only=True)
    browse_time = serializers.SerializerMethodField(
        source='listing_browse_time')
    view_time = serializers.SerializerMethodField(source='listing_view_time')
    ratings = serializers.SerializerMethodField()
    num_ratings = serializers.SerializerMethodField()
    average_rating = serializers.ReadOnlyField(source='get_average_rating')
    likes = serializers.ReadOnlyField(source='listing_reactions.likes')
    disliked = serializers.ReadOnlyField(source='listing_reactions.dislikes')
    taglist = TagRelatedField(many=True, required=False, source='tags')
    reviews = serializers.SerializerMethodField()
    num_reviews = serializers.SerializerMethodField()
    listing_images = serializers.SerializerMethodField()
    amenities = serializers.SerializerMethodField()
    created_at = serializers.SerializerMethodField()
    updated_at = serializers.SerializerMethodField()

    def get_owner_info(self, obj):
        return {
            'username': obj.owner.username,
            'fullname': obj.owner.get_full_name,
            'about_me': obj.owner.profile.about_me,
            'profile_photo': obj.owner.profile.profile_photo.url,
            'email': obj.owner.email,
        }

    def get_listing_images(self, obj):
        images = obj.images.all()
        serializer = ListingImageSerializer(images, many=True)
        return serializer.data

    def get_amenities(self, obj):
        amenities = obj.amenities.all()
        serializer = AmenitiesSerializer(amenities, many=True)
        return serializer.data

    def get_created_at(self, obj):
        now = obj.created_at
        formatted_date = now.strftime('%m%d%Y, %H:%M:%S')
        return formatted_date

    def get_updated_at(self, obj):
        then = obj.updated_at
        formatted_date = then.strftime('%m%d%Y, $H:%M:%S')
        return formatted_date

    def get_ratings(self, obj):
        ratings = obj.listing_ratings.all()
        serializer = RatingSerializer(ratings, many=True)
        return serializer.data

    def get_num_ratings(self, obj):
        num_ratings = obj.listing_ratings.all().count()
        return num_ratings

    def get_reviews(self, obj):
        reviews = obj.reviews.all()
        serializer = ReviewListSerializer(reviews, many=True)
        return serializer.data

    def get_num_reviews(self, obj):
        num_reviews = obj.reviews.all().count()
        return num_reviews

    class Meta:
        model = Listing
        fields = [
            'id', 'title',
            'slug', 'taglist',
            'description',
            'rate', 'availability',
            'view_time', 'owner_info',
            'likes', 'dislikes', 'ratings',
            'num_ratings', 'average_rating',
            'views', 'reviews', 'num_reviews',
            'city', 'listing_images', 'amenities',
            'created_at', 'updated_at',
        ]


class ListingCreateSerializer(serializers.ModelSerializer):
    tags = TagRelatedField(many=True, required=False)
    images = serializers.SerializerMethodField()

    class Meta:
        model = Listing
        exclude = ['pkid', 'updated_at']

    def get_created_at(self, obj):
        now = obj.created_at
        formatted_date = now.strftime('%m/%d/$Y, %H:%M:%S')
        return formatted_date

    def get_product_images(self, obj):
        images = obj.images.all()
        serializer = ListingImageSerializer(images, many=True)
        return serializer.data


class ListingsUpdateSerializer(serializers.ModelSerializer):
    tag = TagRelatedField(many=True, required=False)
    updated_at = serializers.SerializerMethodField()

    def get_updated_at(self, obj):
        then = obj.updated_at
        formatted_date = then.strftime('%m/%d/%Y, $H:%M:%S')
        return formatted_date

    class Meta:
        model = Listing
        fields = ['title', 'description', 'images', 'tags', 'updated_at']
