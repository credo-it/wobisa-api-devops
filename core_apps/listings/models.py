from autoslug import AutoSlugField
from django.contrib.auth import get_user_model
from django.db import models
from django.db.models import Avg
from django.utils.translation import gettext_lazy as _
from core_apps.common.models import TimeStampedUUIDModel
from core_apps.ratings.models import Rating

from .view_time_engine import ListingBrowseTimeEngine

User = get_user_model()


class Tag(TimeStampedUUIDModel):
    tag = models.CharField(max_length=80)
    slug = models.SlugField(db_index=True, unique=True)

    class Meta:
        ordering = ['tag']

    def __str__(self):
        return self.tag


class ListingCategory(models.Model):
    title = models.CharField(max_length=255)
    description = description = models.TextField()

    class Meta:
        ordering = ['title']

    def __str__(self):
        return self.title


class Listing(TimeStampedUUIDModel):
    IN_PROGRESS = 'IN_P'
    LISTED = 'L'
    UNLISTED = 'UL'

    STATUS_CHOICES = [
        (IN_PROGRESS, 'Available'),
        (LISTED, 'Rennovating'),
        (UNLISTED, 'UNLISTED'),
    ]
    owner = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name=_(
        'user'), related_name='listings')
    title = models.CharField(max_length=255, verbose_name=_('title'))
    slug = AutoSlugField(populate_from='title',
                         always_update=True, unique=True)
    description = models.CharField(
        max_length=255, verbose_name=_('description'))
    status = models.CharField(
        max_length=255, verbose_name=_('availability'), choices=STATUS_CHOICES, default=IN_PROGRESS)
    rate = models.DecimalField(
        max_digits=6, verbose_name=_('rate'), decimal_places=2)
    location_log = models.DecimalField(
        max_digits=6, decimal_places=2, default=00000)
    location_lat = models.DecimalField(max_digits=6, decimal_places=2)
    tag = models.ManyToManyField(Tag, related_name='listings')
    views = models.IntegerField(verbose_name='listing views', default=0)
    city = models.CharField(max_length=255, verbose_name=_('city'))
    category = models.ForeignKey(
        ListingCategory, on_delete=models.PROTECT, related_name='listings')

    def __str__(self):
        return self.title

    @property
    def list_of_tags(self):
        tags = [tag.tag for tag in self.tags.all()]
        return tags

    @property
    def listing_browse_time(self):
        time_to_browse = ListingBrowseTimeEngine(self)
        return time_to_browse.get_view_time()

    @property
    def get_average_rating(self):
        if Rating.objects.all().count > 0:
            rating = (
                Rating.objects.filter(
                    listing=self.pkid).all().aggregate(Avg=('value'))
            )
            return round(rating['value__averag'], 1) if rating['value__avg'] else 0
            return 0

    class Meta:
        ordering = ['title']


class ListingViews(TimeStampedUUIDModel):
    ip = models.CharField(max_length=255, verbose_name=_('ip addresses'))
    listing = models.ForeignKey(
        Listing, related_name='listing_views', on_delete=models.CASCADE)

    def __str__(self):
        return (
            f'Total views on - {self.listing.title} is - {self.listing.listing_views} view(s)'
        )

    class Meta:
        verbose_name = 'Total views on Listing'
        verbose_name_plural = 'Total Listing views'


class ListingImage(models.Model):
    listing = models.ForeignKey(Listing, on_delete=models.CASCADE, verbose_name=_(
        'listing_images'), related_name='images')
    listing_image = models.ImageField(verbose_name=_(
        'listing images'), default='/house_sample.jpg')


class Amenities(models.Model):
    type = models.CharField(max_length=255, verbose_name='type_of_amenity')
    listing = models.ForeignKey(
        Listing, on_delete=models.CASCADE, related_name='amenities')
    description = models.CharField(
        max_length=255, verbose_name='amenities description')
    quantity = models.PositiveIntegerField(verbose_name=_('quantity'))
