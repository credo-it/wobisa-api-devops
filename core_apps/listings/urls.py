from django.urls import path
from .views import (
    ListingListAPIView,
    ListingCreateAPIView,
    ListingDeleteAPIView,
    ListingDetailView,
    update_listing_api_view
)

urlpatterns = [
    path('all/', ListingListAPIView.as_view(), name='all-listings'),
    path('create/', ListingCreateAPIView.as_view(), name='create-listings'),
    path('details/<slug:slug>/', ListingDetailView.as_view(),
         name='listing-details'),
    path('delete/<slug:slug>/', ListingDeleteAPIView.as_view(),
         name='delete-listing'),
    path('upate/<slug:slug>/', update_listing_api_view, name='update-listing')
]
