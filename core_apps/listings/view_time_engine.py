class ListingBrowseTimeEngine:

    def __init__(self, listing):
        self.listing = listing
        self.time_spent_on_listing = 52

    def check_listing_has_display_images(self):
        has_display_images = True
        if not self.listing.display_images:
            has_display_images = False
            self.display_image_adjustment_time = 0
        return has_display_images

    def get_title(self):
        return self.listing.title

    def get_rate(self):
        return self.listing.rate

    def get_tags(self):
        tag_words = []
        [tag_words.extend(tag_word.split())
         for tag_word in self.listing.list_tags]
        return tag_words

    def get_description(self):
        return self.listing.description

    def get_listing_details(self):
        details = []
        details.extend(self.get_title().split())
        details.extend(self.get_description.split())
        details.extend(self.get_tags())
        details.extend(self.get_rate.split())
        return details

    def get_view_time(self):
        view_time = 0
        view_duration = len(self.get_listing_details())
        self.check_listing_has_display_images()

        if view_duration:
            time_to_browse = view_duration / self.time_spent_on_listing
            if time_to_browse < 1:
                view_time = (
                    str(round((time_to_browse + self.display_image_adjustment_time) * 60))
                    + " second(s)"
                )
            else:
                view_time = (str(
                    round(time_to_browse + self.display_image_adjustment_time)) + " minute(s)")
                return view_time
