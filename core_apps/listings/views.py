import logging
from django.contrib.auth import get_user_model
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, generics, permissions, status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.exceptions import NotFound
from rest_framework.response import Response
from rest_framework.views import APIView

from core_apps.listings.models import (
    Listing, ListingImage,
    ListingViews, Amenities
)
from .exceptions import UpdateListingException
from .filters import ListingFilter
from .pagination import ListingPagination
from .permissions import IsOwnerOrReadonly
from .renderers import ListingsSONRenderer, ListingsJSONRenderer
from .serializers import (
    ListingSerializer,
    ListingsUpdateSerializer,
    ListingCreateSerializer
)

User = get_user_model()
logger = logging.getLogger(__name__)


class ListingListAPIView(generics.ListAPIView):
    serializer_class = ListingSerializer
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    queryset = Listing.objects.all()
    renderer_classes = (ListingsSONRenderer,)
    pagination_class = ListingPagination
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filterset_class = ListingFilter
    ordering_fields = ['city', 'rate']


class ListingCreateAPIView(generics.CreateAPIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = ListingCreateSerializer
    renderer_class = [ListingsJSONRenderer]

    def create(self, request, *args, **kwargs):
        user = request.user
        data = request.data
        data['owner'] = user.pkid
        serializer = self.serializer_class(
            data=data, context={'request': request})
        serializer.save()
        logger.info(
            f'Listing {serializer.data.get("title")} created by {user.username}'
        )
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class ListingDetailView(APIView):
    renderer_class = [ListingsSONRenderer]
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, slug):
        listing = Listing.objects.get(slug=slug)
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')
        if not ListingViews.objects.filter(listing=listing, ip=ip).exists():
            ListingViews.objects.create(listing=listing, ip=ip)
            listing.views += 1
            listing.save()

        serializer = ListingSerializer(listing, context={'request': request})

        return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['PATCH'])
@permission_classes([permissions.IsAuthenticated])
def update_listing_api_view(request, slug):
    try:
        listing = Listing.objects.get(slug=slug)
    except Listing.DoesNotExist:
        raise NotFound(
            'The listing you looking for does not exist in our databse.')

    user = request.user
    if listing.owner != user:
        raise UpdateListingException

    if request.method == 'PATCH':
        data = request.data
        serializer = ListingsUpdateSerializer(listing, data=data, many=False)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)


class ListingDeleteAPIView(generics.DestroyAPIView):
    permission_classes = [permissions.IsAuthenticated, IsOwnerOrReadonly, ]
    queryset = Listing.objects.all()
    lookup_field = 'slug'

    def delete(self, request, *args, **kwargs):
        try:
            listing = Listing.objects.get(slug=self.kwargs.get('slug'))
        except Listing.DoesNotExist:
            raise NotFound(
                'The listing you\'re requesting for does not exist in our database.')

        delete_operation = self.destroy(request)
        data = {}
        if delete_operation:
            data['SUCCESS'] = 'Deletetion was successful.'
        else:
            data['failuer'] = 'deletion failed.'

        return Response(data=data)
