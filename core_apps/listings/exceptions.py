from rest_framework.exceptions import APIException


class UpdateListingException(APIException):
    satus_code = 403
    default_detail = 'You can\'t update a listing that does not belong to you.'
