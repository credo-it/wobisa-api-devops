import django_filters as filters
from core_apps.listings.models import Listing


class ListingFilter(filters.FilterSet):
    owner = filters.CharFilter(
        field_name='owner__first_name', lookup_expr='icontains')
    title = filters.CharFilter(field_name='title', lookup_expr='iexact')
    tags = filters.CharFilter(
        field_name='tags', method='get_listing_tags', lookup_expr='iexact')
    city = filters.CharFilter(field_name='city', lookup_expr='icontains')

    rate = filters.NumberFilter()
    rate__gt = filters.NumberFilter(field_name='rate', lookup_expr='gt')
    rate__lt = filters.NumberFilter(field_name='rate', looup_expr='lt')

    class Meta:
        model = Listing
        fields = ['title', 'tags', 'rate', 'city']

    def get_listing_tags(self, queryset, tags, value):
        tag_values = value.replace(' ', '').split(',')
        return queryset.filter(tags__tag__in=tag_values).distinct()
