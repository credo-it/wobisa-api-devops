from rest_framework import permissions


class IsOwnerOrReadonly(permissions.BasePermission):
    message = 'You are not allowed to update a listing that does not belong to you.'

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.owner == request.user


class IsProviderOrReadonly(permissions.BasePermission):
    message = 'You must follow the process to be able to make listings.'

    def has_listing_permissions(self, request, view, obj):
        if request.user.is_service_provide:
            return True
        else:
            return False
