from django.conf import settings
from django.contrib import admin
from django.urls import path, include
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

# ///////// FOR SWAGGER AND REDOC API DOCUMENTATION ////////////////////
schema_view = get_schema_view(
    openapi.Info(
        title='Wobisa API',
        default_version='v1',
        description='API endpoints for the Wobisa API',
        contact=openapi.Contact(email='info@credo-itsolutions.com'),
        license=openapi.License(name='MIT License')

    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)
# ///////// FOR SWAGGER AND REDOC API DOCUMENTATION ////////////////////

urlpatterns = [
    path('swagger/', schema_view.with_ui('swagger',
         cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc',
         cache_timeout=0), name='schema-redoc'),

    path(settings.ADMIN_URL, admin.site.urls),
    path('api/v1/auth/', include('djoser.urls')),
    path('api/v1/auth/', include('djoser.urls.jwt')),
    path('api/v1/profiles/', include('core_apps.profiles.urls')),
    path('api/v1/listings/', include('core_apps.listings.urls')),
    path('api/v1/ratings/', include('core_apps.ratings.urls')),
    path('api/v1/rate/', include('core_apps.reactions.urls')),
    path('api/v1/favourite/', include('core_apps.favourites.urls')),
    path('api/v1/reviews/', include('core_apps.reviews.urls')),
    path('api/v1/haystack/', include('core_apps.search.urls')),
    # path('app/v1/kyc/', include('core_apps.kyc.urls')),
]


admin.site.site_header = 'Wobisa API Admin'
admin.site.site_title = 'Wobisa API Admin Portal'
admin.site.index_title = 'Welcome to the Wobisa API Portal'
