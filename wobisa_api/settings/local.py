from .base import *
from .base import env

DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env('DJANGO_SECRET_KEY',
                 default='vpf_h@!cdc8t!^&5w=5qzjy&#b!73tyj+=!07cnlh58t*onh2h')
# 'django-insecure-vpf_h@!cdc8t!^&5w=5qzjy&#b!73tyj+=!07cnlh58t*onh2h'

ALLOWED_HOSTS = ['localhost', '0.0.0.0', '127.0.0.1']


EMAIL_BACKEND = "djcelery_email.backends.CeleryEmailBackend"
EMAIL_HOST = env("EMAIL_HOST", default="mailhog")
EMAIL_PORT = env("EMAIL_PORT")
DEFAULT_FROM_EMAIL = "info@credo-itsolutions.com"
DOMAIN = env("DOMAIN")
SITE_NAME = "Wobisa App"
