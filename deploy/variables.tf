variable "prefix" {
  default = "wad"
}

variable "project" {
  default = "wobisa-api-devops"
}

variable "contact" {
  default = "ecobblah2@gmail.com"
}