build:
	docker compose -f local.yml up --build -d --remove-orphans

up:
	docker compose -f local.yml up -d


down:
	docker compose -f local.yml down


show_logs:
	docker compose -f local.yml logs


makemigrations:
	docker compose -f local.yml run --rm api python3 manage.py makemigrations


migrate:
	docker compose -f local.yml run --rm api python3 manage.py migrate


collectstatic:
	docker compose -f local.yml run --rm api python3 manage.py collectstatic --no-input --clear

create_superuser:
	docker compose -f local.yml run --rm api python3 manage.py createsuperuser


remove_containers_volumes:
	docker compose -f local.yml down -v


inspect_volume:
	docker volume inspect dwumapa-src_local_postgres_data


connect_to_db_service:
	docker compose -f local.yml exec postgres psql --username=eyram --dbname=dwumapa


flake8:
	docker compose -f loca.yml exec api flake8 .


black_check:
	docker compose -f local.yml exec api black --check --exclude=migrations .


black_diff:
	docker compose -f local.yml exec black api black --diff --exclude=migrations .


black:
	docker compose -f local.yml exec api black --exclude=migratins .


isort_check:
	docker compose -f local.yml exec api isort . --check-only --skip env --skip migrations


isort_diff:
	docker compose -f local.yml exec isort . --diff --skip env --skip migrations


isort:
	docker compose -f local.yml exec isort . --skip env --skip mirgations


#####################################################
# Makefile containing shortcut commands for project #
#####################################################

# MACOS USERS:
#  Make should be installed with XCode dev tools.
#  If not, run `xcode-select --install` in Terminal to install.

# WINDOWS USERS:
#  1. Install Chocolately package manager: https://chocolatey.org/
#  2. Open Command Prompt in administrator mode
#  3. Run `choco install make`
#  4. Restart all Git Bash/Terminal windows.

.PHONY: tf-init
tf-init:
	docker-compose -f deploy/docker-compose.yml run --rm terraform init

.PHONY: tf-fmt
tf-fmt:
	docker-compose -f deploy/docker-compose.yml run --rm terraform fmt

.PHONY: tf-validate
tf-validate:
	docker-compose -f deploy/docker-compose.yml run --rm terraform validate

.PHONY: tf-plan
tf-plan:
	docker-compose -f deploy/docker-compose.yml run --rm terraform plan

.PHONY: tf-apply
tf-apply:
	docker-compose -f deploy/docker-compose.yml run --rm terraform apply

.PHONY: tf-destroy
tf-destroy:
	docker-compose -f deploy/docker-compose.yml run --rm terraform destroy

.PHONY: tf-workspace-dev
tf-workspace-dev:
	docker-compose -f deploy/docker-compose.yml run --rm terraform workspace select dev

.PHONY: tf-workspace-staging
tf-workspace-staging:
	docker-compose -f deploy/docker-compose.yml run --rm terraform workspace select staging

.PHONY: tf-workspace-prod
tf-workspace-prod:
	docker-compose -f deploy/docker-compose.yml run --rm terraform workspace select prod

.PHONY: test
test:
	docker-compose run --rm app sh -c "python manage.py wait_for_db && python manage.py test && flake8"


